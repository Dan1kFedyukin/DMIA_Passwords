import numpy as np
from flask import Flask, request, render_template, send_file, abort
import os

from processor import password_model

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))


@app.route('/', methods=['GET', 'POST'])
def password_rate():
    if request.method == 'GET':
        return render_template('main.html')

    if request.method == 'POST':
        password = request.form['password']
        xg_model = password_model.XGBoostPassword()
        prediction = xg_model.predict(password)
        return render_template('main.html', password=password, prediction=np.exp(prediction[0]))


if __name__ == '__main__':
    app.run(threaded=False)
