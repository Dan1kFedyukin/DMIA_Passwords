import numpy as np
import os
import json
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
import xgboost as xgb


class XGBoostPassword:
    def __init__(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        model_file = os.path.join(dir_path, "model_files", "xgboost_model.pickle")
        vec_file = open(os.path.join(dir_path, "model_files", "tf_vectorizer_model.pickle"), 'rb')
        self._xgboost_password_model = xgb.XGBRegressor()
        self._xgboost_password_model.load_model(model_file)
        self._vectorizer = pickle.load(vec_file)

    @staticmethod
    def fit(data):
        vectorizer = TfidfVectorizer(lowercase = False,
                                     analyzer = 'char',
                                     use_idf = False,
                                     sublinear_tf = True,
                                     ngram_range = (1, 4),
                                     max_features = 50000)
        prep_data = vectorizer.fit_transform(data['Password'])
        x_train, x_test, y_train, y_test = train_test_split(prep_data, np.log(data['Times']), test_size = 0.33,
                                                            random_state = 42)
        xg = xgb.XGBRegressor(n_estimators = 150, max_depth = 3, learning_rate = 0.3)
        xg.fit(x_train, y_train)
        xg_pred = xg.predict(x_test)
        rmse = np.sqrt(mean_squared_error(y_test, xg_pred))
        xg.save_model('xgboost_model.pickle')
        file = open(os.path.join("processor/model_files/tf_vectorizer_model.pickle"), 'wb')
        pickle.dump(vectorizer, file)
        file.close()
        print(rmse)

    def predict(self, data, download=False):
        if not download:
            prep_data = self._vectorizer.transform([data])
            return self._xgboost_password_model.predict(prep_data)
        else:
            prep_data = self._vectorizer.transform([data])
            prediction = self._xgboost_password_model.predict(prep_data)
            prediction_dict = {'password': data,
                               'frequency': float(np.exp(prediction[0]))}
            with open('result.json', 'w') as fp:
                json.dump(prediction_dict, fp)
            return prediction
