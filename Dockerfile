FROM python:3.7-slim
ARG PROJNAME=app
ENV PROJNAME=${PROJNAME}
RUN mkdir /${PROJNAME}
WORKDIR /${PROJNAME}

# python packages
RUN pip install pipenv
COPY Pipfile .
COPY Pipfile.lock .
COPY requirements.txt ${PROJNAME}/requirements.txt
RUN pip install --no-cache-dir -r ${PROJNAME}/requirements.txt

COPY . /app
WORKDIR /app

ENV FLASK_APP main.py
CMD ["flask", "run", "--host=0.0.0.0"]
